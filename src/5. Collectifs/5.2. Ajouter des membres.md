Ajouter des membres
===================

1.  Affichez la liste des collectifs en cliquant sur le bouton de menu à gauche

    ![](liste-collectifs.png?fileId=5989#mimetype=image%2Fpng&hasPreview=true)

2.  Cliquez sur le menu d'action correspondant à ce collectif.

    ![](menu-action-collectifs.png?fileId=6010#mimetype=image%2Fpng&hasPreview=true)

3.  Sélectionnez "Gérer les membres" dans le menu déroulant.

    ![](gerer-membres.png?fileId=6045#mimetype=image%2Fpng&hasPreview=true)

4.  Nextcloud renvoie automatiquement sur le module **Contacts**.
    Cliquez sur "Ajouter des membres" dans le menu à gauche de la vue principale.

    -   Le module créé automatiquement un **Cercle** du même nom que le
        collectif. C'est à celui-ci que les membres sont ajouté⋅es.

        ![](ajouter-membres.png?fileId=10625#mimetype=image%2Fpng&hasPreview=true)

5.  Dans la boîte de dialogue qui apparaît, commencez à taper le nom de membres à ajouter et cliquez dessus pour
    les sélectionner.

    ![](chercher-membres.png?fileId=10669#mimetype=image%2Fpng&hasPreview=true)

6.  Cliquez sur "Ajouter à \[nom de votre collectif\]" pour terminer.

    -   Vous pouvez ajouter des contacts (inscrit⋅es sur l'instance de
        Coopaname) ou des cercles entiers.
    -   Pour gérer les rôles et les paramètres du Cercle (et par
        extension, du Collectif), voir la fiche [Créer un
        Cercle](<../6. Contacts/6.2. Créer un Cercle.md>).

------------------------------------------------------------------------

➡️ Continuer en [créant une nouvelle
page](<../5. Collectifs/5.3. Créer des pages.md>).