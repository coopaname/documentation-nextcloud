Créer des pages
===============

Créer une page
--------------

-   Cliquez sur le bouton **+** à côté du nom de votre collectif dans le menu à gauche.

    ![](creer-page.png?fileId=6094#mimetype=image%2Fpng&hasPreview=true)

-   Une nouvelle page apparaît dans la liste.

-   Nommez la page dans le champ texte en haut de la vue principale.

    ![](nommer-page.png?fileId=13065#mimetype=image%2Fpng&hasPreview=true)

-   L'enregistrement est automatique.

-   Chaque page a son propre bouton **+** qui permet de créer une
    **arborescence** dans laquelle organiser le travail.

-   Les pages sont organisées par date de dernière modification ou par
    ordre alphabétique. Pour changer l'affichage, cliquez sur le bouton
    en haut de l'arborescence.

    ![](arborescence.png?fileId=13049#mimetype=image%2Fpng&hasPreview=true)

Modifier une page
-----------------

1.  Cliquez sur la page que vous souhaitez modifier dans la liste.

2.  Cliquez sur le bouton "Modifier" à droite du titre, en haut de la vue principale.

    ![](modifier.png?fileId=6112#mimetype=image%2Fpng&hasPreview=true)

3.  L'enregistrement est automatique. Vous pouvez vérifier en regardant
    l'état affiché en haut de l'éditeur de texte.

    -   "Enregistrement…"

    -   "Enregistré"

        ![](enregistrement.png?fileId=13150#mimetype=image%2Fpng&hasPreview=true)

        ![](enregistre.png?fileId=13149#mimetype=image%2Fpng&hasPreview=true)

4.  Quand vous avez terminé l'édition de la page, cliquez sur le bouton
    "Terminé" à droite du titre.

    ![](termine.png?fileId=13151#mimetype=image%2Fpng&hasPreview=true)

L'ensemble des pages créées se retrouve dans un **dossier "Collectifs"**
du module **Fichiers**, organisées en dossiers, sous-dossiers et
documents texte.

![](dossier-collectifs.png?fileId=13222#mimetype=image%2Fpng&hasPreview=true)

![](arborescence-dossiers.png?fileId=13213#mimetype=image%2Fpng&hasPreview=true)

------------------------------------------------------------------------

➡️ Continuer en découvrant le module
[Contacts](<../6. Contacts/Readme.md>).