Publier à l\'extérieur
======================

Pour **rendre un contenu public** (accessible sans compte sur l'espace
de Coopaname) :

1.  Dans la liste des fichiers, cliquez sur l'icône de partage du
    fichier que vous souhaitez publier.

    -   La fenêtre "Détails" s'ouvre sur la partie droite de l'écran.

        ![](menu-partage.png?fileId=9191#mimetype=image%2Fpng&hasPreview=true)

2.  Cliquez sur le bouton **+** de la ligne "Lien de partage".

    ![](lien-partage.png?fileId=9707#mimetype=image%2Fpng&hasPreview=true)

    -   Le lien est copié automatiquement dans votre presse-papier.

3.  Transmettez le liens aux destinataires de la manière de votre choix.

**Dans le cas d'un dossier, les destinataires ont accès à tout son
contenu**, y compris les sous-dossiers.

Par défaut, les destinataires d'un partage extérieur ne peuvent que
**lire et télécharger** le document ou le dossier partagé. Il est
possible de modifier ces autorisations.

> ⚠️ Ce lien est **public** et peut circuler partout, à moins que vous
> ne révoquiez son partage (voir "Autorisations").

Autorisations pour un document
------------------------------

1.  Cliquez sur le menu d'action sur la ligne "Lien de partage".

    ![](acces-exterieur-document.png?fileId=9739#mimetype=image%2Fpng&hasPreview=true)

2.  Cochez ou décochez les différents paramètres dans le menu déroulant selon votre souhait.

    -   *Autoriser la modification* : les destinataires peuvent modifier
        le contenu du fichier (mais pas le supprimer).
    -   *Masquer le téléchargement* : empêche les destinataires de
        télécharger le document.
    -   *Protéger par un mot de passe* : ajoutez un mot de passe à votre
        lien.
    -   *Définir une date d'expiration* : le partage sera
        automatiquement révoqué à la date de votre choix.
    -   *Note au destinataire* : précisez une courte information.
    -   *Ne plus partager* : révoquez le partage de ce fichier.
    -   *Ajouter un autre lien* : créer un deuxième lien de partage
        (utile pour avoir des liens avec des autorisations différentes).

Autorisations pour un dossier
-----------------------------

1.  Cliquez sur le menu d'action sur la ligne du ou de la destinataire
    du document.

    ![](acces-exterieur-dossier.png?fileId=9740#mimetype=image%2Fpng&hasPreview=true)

2.  Cochez ou décochez les différents paramètres dans le menu déroulant selon votre souhait.

    -   *Lecture seule* : les destinataires ne peuvent que lire le
        contenu du dossier.

    -   *Autoriser l'ajout et la modification* : les destinataires
        peuvent modifier le contenu du dossier et y ajouter des
        documents ou sous-dossier (mais pas le supprimer).

    -   *Dépôt de fichier* : les destinataires peuvent déposer des
        documents ou dossiers, mais ne voient pas le contenu du dossier.

        ![](depot-fichiers.png?fileId=9837#mimetype=image%2Fpng&hasPreview=true)

    -   *Masquer le téléchargement* : empêche les destinataires de
        télécharger le dossier et son contenu.

    -   *Protéger par un mot de passe* : ajoutez un mot de passe à votre
        lien.

    -   *Définir une date d'expiration* : le partage sera
        automatiquement révoqué à la date de votre choix.

    -   *Note au destinataire* : précisez une courte information.

    -   *Ne plus partager* : révoquez le partage de ce fichier.

    -   *Ajouter un autre lien* : créer un deuxième lien de partage
        (utile pour avoir des liens avec des autorisations différentes).

------------------------------------------------------------------------

➡️ Continuer en [discutant à travers
Nextcloud](<../4. Discuter/Readme.md>).
