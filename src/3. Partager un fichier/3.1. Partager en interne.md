Partager en interne
===================

Pour **partager un contenu avec une autre personne de Coopaname**
(inscrite sur Nextcloud) :

1.  Dans la liste des fichiers, cliquez sur l'icône de partage du
    fichier que vous souhaitez partager.

    -   La fenêtre "Détails" s'ouvre sur la partie droite de l'écran.

        ![](menu-partage.png?fileId=9191#mimetype=image%2Fpng&hasPreview=true)

2.  Dans le champ de saisie commencez à écrire le nom de la personne ou
    du cercle à qui vous voulez partager votre fichier.

    ![](selectionner-nom-partage.png?fileId=9201#mimetype=image%2Fpng&hasPreview=true)

3.  Cliquez sur le nom qui apparaît.

    -   Les destinataires apparaissent dans une liste sous le champ de
        saisie.

        ![](liste-partages.png?fileId=9183#mimetype=image%2Fpng&hasPreview=true)

Une fois le partage effectué, les destinataires retrouvent le document
ou le dossier dans leur espace personnel. Il est aussi possible de leur
**communiquer un lien interne**, permettant l'accès direct. Pour générer
ce lien :

1.  Cliquez sur l'icône de presse-papier au bout de la ligne "lien interne" dans la fenêtre de détails.
    -   le lien est automatiquement copié dans votre presse-papier.

        ![](lien-interne.png?fileId=9644#mimetype=image%2Fpng&hasPreview=true)
2.  Transmettez le liens aux destinataires de la manière de votre choix.

Les destinataires peuvent **renommer** et **déplacer** le dossier ou le
document dans leur espace sans que la modification affecte votre copie.

**Dans le cas d'un dossier, les destinataires ont accès à tout son
contenu**, y compris les sous-dossiers.

Par défaut, les destinataires d'un partage interne ont la possibilité de
**modifier et repartager le contenu** du document ou du dossier partagé.
Il est possible de modifier ces autorisations.

Autorisations pour un document
------------------------------

1.  Cliquez sur le menu d'action sur la ligne du ou de la destinataire
    du document.

    ![](acces-document.png?fileId=9658#mimetype=image%2Fpng&hasPreview=true)

2.  Cochez ou décochez les différents paramètres dans le menu déroulant selon votre souhait.

    -   *Autoriser la modification* : les destinataires peuvent modifier
        le contenu du fichier (mais pas le supprimer).
    -   *Autoriser le repartage* : les destinataires peuvent partager le
        fichier en interne ou à l'extérieur.
    -   *Définir une date d'expiration* : le partage sera
        automatiquement révoqué à la date de votre choix.
    -   *Note au destinataire* : précisez une courte information.
    -   *Ne plus partager* : révoquez le partage de ce fichier.

Autorisations pour un dossier
-----------------------------

1.  Cliquez sur le menu d'action sur la ligne du ou de la destinataire
    du document.

    ![](acces-dossier.png?fileId=9659#mimetype=image%2Fpng&hasPreview=true)

2.  Cochez ou décochez les différents paramètres dans le menu déroulant selon votre souhait.

    -   *Autoriser la modification* : les destinataires peuvent modifier
        le contenu du dossier (mais pas le supprimer).
    -   *Autoriser la création* : les destinataires peuvent créer de
        nouveaux documents ou dossiers dans le dossier partagé.
    -   *Autoriser la suppression* : les destinataires peuvent supprimer
        des éléments contenus dans le dossier partagé (non recommandé).
    -   *Autoriser le repartage* : les destinataires peuvent partager le
        fichier en interne ou à l'extérieur.
    -   *Définir une date d'expiration* : le partage sera
        automatiquement révoqué à la date de votre choix.
    -   *Note au destinataire* : précisez une courte information.
    -   *Ne plus partager* : révoquez le partage de ce fichier.

------------------------------------------------------------------------

➡️ Continuer en [partageant à
l'extérieur](<../3. Partager un fichier/3.2. Publier à l'extérieur.md>).