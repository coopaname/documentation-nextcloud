Partager un fichier
===================

Nextcloud permet de partager un document ou un dossier avec d'autres
personnes, au sein de Coopaname ou à l'extérieur. Les membres de
Coopaname auront accès à ce fichier avec un **lien interne.** Pour une
personne extérieure, comme un⋅e client⋅e par exemple, l'accès se fait
avec un **lien de partage**.

Si un fichier est partagé avec autorisation de modification, alors les
destinataires pourront modifier le fichier ou le contenu du dossier. Il
est possible de partager un fichier ou un dossier sans possibilité de
modification (lecture seule).

**Lorsqu'un dossier est partagé, tout son contenu l'est également.**

------------------------------------------------------------------------

➡️ Continuer en [partageant un contenu en
interne](<../3. Partager un fichier/3.1. Partager en interne.md>).
