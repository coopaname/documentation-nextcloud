Synchroniser ses discussions
============================

L'installation de l'application Nextcloud Talk se fait de la même
manière sur **iPhone** et sur **Android**.

1.  Recherchez l'application "Nextcloud Talk" dans votre magasin
    habituel d'applications.

    ![](application-nextcloud-talk.png?fileId=35573#mimetype=image%2Fpng&hasPreview=true)

2.  Installez l'application puis ouvrez-la.

3.  Entrez l'adresse du Nextcloud de Coopaname dans le champ de saisie :
    nextcloud.coopaname.coop

    <p><img src="adresse-nextcloud-talk.png?fileId=35582#mimetype=image%2Fpng&amp;hasPreview=true" alt="" width="300"/></p>

4.  Appuyez sur "Se connecter" sur la page de Coopaname.

    <p><img src="coopaname-se-connecter.png?fileId=10058#mimetype=image%2Fpng&amp;hasPreview=true" alt="" width="300"/></p>

5.  Entrez l'adresse mail et le mot de passe utilisés pour le portail de
    Coopaname et cliquez sur "Se connecter".

    <p><img src="coopaname-mot-de-passe.png?fileId=10059#mimetype=image%2Fpng&amp;hasPreview=true" alt="" width="300"/></p>

6.  Appuyez sur "Autoriser l'accès".

    <p><img src="coopaname-autoriser-nextcloud.png?fileId=10078#mimetype=image%2Fpng&amp;hasPreview=true" alt="coopaname-autoriser-nextcloud.png" width="300"/></p>

Vous avez désormais accès à vos discussions Nextcloud depuis votre
mobile. Vous pouvez y réaliser les mêmes actions que sur l'interface
Web.

<p><img src="accueil-nextcloud-talk.png?fileId=35591#mimetype=image%2Fpng&amp;hasPreview=true" alt="" width="300"/></p>

------------------------------------------------------------------------

➡️ Continuer en [synchronisant vos contacts et vos agendas](<../8. Nextcloud sur mobile/8.3. Synchroniser ses contacts et agendas.md>).