Introduction
============

Qu'est-ce que Nextcloud ?
-------------------------

[Nextcloud](https://www.nextcloud.com/fr_FR) est un logiciel permettant de
**stocker** sur un serveur des contenus (fichiers, dossiers, documents,
vidéos, ...) et de les **partager** facilement avec d'autres personnes.

Il permet aussi de **synchroniser** automatiquement les dossiers de son
espace sur Internet avec son ordinateur sous Windows, Mac ou Linux, ou
son téléphone sous iOS ou Android. On peut ainsi accéder à ses fichiers
à tout moment, revenir sur les anciennes version, retrouver un fichier
effacé etc.

À Coopaname, nous utilisons Nextcloud pour **faciliter la
collaboration** entre les personnes qui font la coopérative :
entrepreneur⋅ses salarié⋅es, membres de l'équipe mutualisée, associé⋅es,
personnes en contrat CAPE.

En plus du stockage et partage de contenus, Nextcloud propose un
ensemble d'applications disponibles depuis la page d'accueil, permettant
d'organiser un véritable **espace de travail en groupe** et en ligne.

Les modules utilisés à Coopaname (D'autres modules existent, si vous
pensez qu'ils peuvent être utiles sur notre espace partagé, faites signe
à l'équipe informatique) sont :

-   **🗨️ Discussions** : un espace de discussion par écrit (*chat*) avec
    possibilité de visio conférence et de partage d'écran.
-   **👥 Contacts** : un système de gestion de carnets de contacts. Il
    est possible de synchroniser les contacts avec un logiciel tiers sur
    son ordinateur ou une application sur son téléphone.
-   **🗓️ Agenda** : un système de calendriers partageables. Il est
    possible de synchroniser les agendas avec un logiciel tiers sur son
    ordinateur ou une application sur son téléphone.
-   **☑️ Deck** : un système de gestion de projet basé sur la méthode
    [Kanban](https://fr.wikipedia.org/wiki/Kanban).
-   **⭐ Collectifs** : un système d'organisation de groupes de travail.

Tous ces modules peuvent communiquer entre eux et partager des
informations. De même, les différents contenus peuvent être partagés
avec d'autres membres de Coopaname, ou avec des personnes extérieures à
la coopérative.

Des applications de bureautique (avec le logiciel
[Collabora](https://www.collaboraoffice.com/fr/collabora-online/))
permettent l'**édition collaborative de documents** :

-   **Notes**
-   **Traitement de texte**
-   **Feuille de calcul**
-   **Présentation**
-   **Diagramme**

Pourquoi Nextcloud ?
--------------------

Il existe plusieurs services de *cloud* connus et répandus, tels que
Google Drive, Microsoft OneDrive, Dropbox, iCloud, Amazon Drive. Ces
services, tous centralisés et souvent hébergés aux États-Unis reposent
sur l'exploitation des données qui y sont stockées. L'utilisateur⋅ice,
en acceptant les conditions générales d'utilisation, laisse ces
hébergeurs utiliser des données privées et/ou professionnelles en vue
d'une exploitation commerciale.

De plus, les lois de certains gouvernements permettent aux autorités
d'avoir accès aux données très facilement sans même en avertir les
propriétaires.

En quoi Nextcloud est-il différent ?

-   C'est un **logiciel libre** dont le code source est diffusé et donc
    auditable par des développeur⋅euses indépendant⋅es de l'éditeur.
-   C'est un logiciel qui peut être installé sur son propre serveur,
    comme l'a fait Coopaname (on parle de "l'instance de Coopaname").
    **Vous gardez ainsi la main sur vos données** !

> 📗 Une **instance** est un exemplaire (une copie) du logiciel Nextcloud
> installé sur un serveur Web (ici, celui de Coopaname). d'autres
> coopératives, entreprises, associations... peuvent avoir leur propre
> instance Nextcloud. Les différentes instances ne communiquent pas
> entre elles.

À propos de cette documentation
-----------------------------------

\
Ce guide d'utilisation concerne la version 23 de Nextcloud. Il a été
réalisé par La Dérivation et s'appuie sur des documentations déjà
existantes, notamment le travail de Dimitri Robert pour L.A. Coalition,
ainsi que les traductions en français de la documentation officielle de
Nextcloud. 

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Cette documentation est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.

*CC-by-SA Coopaname - La Dérivation - Dimitri Robert*

------------------------------------------------------------------------

➡️ Continuer avec les [Premier pas](<1. Premiers pas/Readme.md>).