Summary
=======

[Introduction](Readme.md)

[Recherche par cas d'usages](0.%20Recherche%20par%20cas%20d'usages.md)

-   [Premiers pas](1.%20Premiers%20pas/Readme.md)
    -   [Se connecter (sur le
        web)](1.%20Premiers%20pas/1.1.%20Se%20connecter%20(sur%20le%20web).md)
    -   [Découvrir la page
        d'accueil](1.%20Premiers%20pas/1.2.%20Découvrir%20la%20page%20d'accueil.md)
    -   [Personnaliser les
        paramètres](1.%20Premiers%20pas/1.3.%20Personnaliser%20les%20paramètres.md)
-   [Gérer ses documents en
    ligne](2.%20Gérer%20ses%20documents%20en%20ligne/Readme.md)
    -   [Créer un nouveau
        dossier](2.%20Gérer%20ses%20documents%20en%20ligne/2.1.%20Créer%20un%20nouveau%20dossier.md)
    -   [Déposer un
        document](2.%20Gérer%20ses%20documents%20en%20ligne/2.2.%20Déposer%20un%20document.md)
    -   [Déplacer des
        documents](2.%20Gérer%20ses%20documents%20en%20ligne/2.3.%20Déplacer%20des%20documents.md)
    -   [Supprimer et restaurer des
        fichiers](2.%20Gérer%20ses%20documents%20en%20ligne/2.4.%20Supprimer%20et%20restaurer%20des%20fichiers.md)
    -   [Restaurer une ancienne version d'un
        document](2.%20Gérer%20ses%20documents%20en%20ligne/2.5.%20Restaurer%20une%20ancienne%20version%20d'un%20document.md)
-   [Partager un fichier](3.%20Partager%20un%20fichier/Readme.md)
    -   [Partager en
        interne](3.%20Partager%20un%20fichier/3.1.%20Partager%20en%20interne.md)
    -   [Publier à
        l'extérieur](3.%20Partager%20un%20fichier/3.2.%20Publier%20à%20l'extérieur.md)
-   [Discuter](4.%20Discuter/Readme.md)
    -   [Créer une
        discussion](4.%20Discuter/4.1.%20Créer%20une%20discussion.md)
    -   [Commencer les
        échanges](4.%20Discuter/4.2.%20Commencer%20les%20échanges.md)
    -   [Discuter autour d'un
        document](4.%20Discuter/4.3.%20Discuter%20autour%20d'un%20document.md)
-   [Collectifs](5.%20Collectifs/Readme.md)
    -   [Créer un
        collectif](5.%20Collectifs/5.1.%20Créer%20un%20collectif.md)
    -   [Ajouter des
        membres](5.%20Collectifs/5.2.%20Ajouter%20des%20membres.md)
    -   [Créer des pages](5.%20Collectifs/5.3.%20Créer%20des%20pages.md)
-   [Contacts](6.%20Contacts/Readme.md)
    -   [Créer un contact](6.%20Contacts/6.1.%20Créer%20un%20contact.md)
    -   [Créer un Cercle](6.%20Contacts/6.2.%20Créer%20un%20Cercle.md)
    -   [Partager un carnet
        d'adresses](6.%20Contacts/6.3.%20Partager%20un%20carnet%20d'adresses.md)
-   [Agenda](7.%20Agenda/Readme.md)
    -   [Gérer ses agendas](7.%20Agenda/7.1.%20Gérer%20ses%20agendas.md)
    -   [Organiser ses
        événements](7.%20Agenda/7.2.%20Organiser%20ses%20événements.md)
    -   [Partager un
        agenda](7.%20Agenda/7.3.%20Partager%20un%20agenda.md)
-   [Nextcloud sur mobile](8.%20Nextcloud%20sur%20mobile/Readme.md)
    -   [Gérer ses fichiers sur
        mobile](8.%20Nextcloud%20sur%20mobile/8.1.%20Gérer%20ses%20fichiers%20sur%20mobile.md)
    -   [Synchroniser ses
        discussions](8.%20Nextcloud%20sur%20mobile/8.2.%20Synchroniser%20ses%20discussions.md)
    -   [Synchroniser ses contacts et
        agendas](8.%20Nextcloud%20sur%20mobile/8.3.%20Synchroniser%20ses%20contacts%20et%20agendas.md)

[Licence](LICENCE.md)
