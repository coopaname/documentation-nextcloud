Partager un carnet d\'adresses
==============================

Vous pouvez **partager** un carnet d'adresses à **d'autres
utilisateur⋅ices** (inscrit⋅es sur l'instance Nextcloud de Coopaname).

1.  Cliquez sur Paramètres en bas à gauche de l'interface.

    ![](parametres-contacts.png?fileId=34961#mimetype=image%2Fpng&hasPreview=true)

2.  Cliquez sur le bouton de partage correspondant au carnet d'adresses
    que vous voulez partager.

    ![](partager-contacts.png?fileId=35315#mimetype=image%2Fpng&hasPreview=true)

3.  Entrez le nom des destinataires dans le champ de saisie qui apparaît et sélectionnez-les dans la liste
    déroulante.

4.  Accordez ou non l'autorisation de modifier le carnet d'adresses, ou
    révoquez le partage.

    ![Case "peut modifier" à cocher et icône corbeille à côté de chaque nom](acces-carnet-adresses.png?fileId=35387#mimetype=image%2Fpng&hasPreview=true)

> ℹ️ Il n'est pas possible de partager directement un carnet d'adresses
> avec l'extérieur, mais vous pouvez le télécharger (format .vcf) et
> ensuite le transmettre par le moyen de votre choix.
> 1. Cliquez sur le menu d'action correspondant au carnet d'adresses.
> 2. Choisissez "Télécharger" dans le menu déroulant.
> 
> ![](telecharger-carnet-adresses.png?fileId=35346#mimetype=image%2Fpng&hasPreview=true)

------------------------------------------------------------------------

➡️ Continuer en découvrant le module [Agenda](<../7. Agenda/Readme.md>).