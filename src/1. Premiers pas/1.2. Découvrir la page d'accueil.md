Découvrir la page d\'accueil
============================

Lorsque vous vous connectez pour la première fois, vous êtes accueilli
par une bannière de présentation de Nextcloud. Vous pouvez cliquer sur
la flèche à droite de la bannière pour visionner les 3 diapositives ou cliquer sur la croix en haut à droite de la page pour la fermer.

![](accueil.png?fileId=6414#mimetype=image%2Fpng&hasPreview=true)

La page d'accueil est celle de vos dossiers et contenus.

<figure>
<img src="dossiers-et-contenus.png?fileId=6458#mimetype=image%2Fpng&hasPreview=true" alt="Page d'accueil de Nextcloud" />
<figcaption><i>La vue principale montre les dossiers et fichiers disponibles sur l'espace. À gauche, un menu de navigation. En haut, une barre d'accès aux différent modules, à gauche, et à la recherche et au profil, à droite.</i></figcaption>
</figure>

Repérez les icônes des modules disponibles. Passez la souris sur la
barre d'icônes pour voir leur nom apparaître.

<video width="640" height="480" controls>
  <source src="survol-modules.mp4" type="video/mp4">
</video>

------------------------------------------------------------------------

➡️ Continuer en [personnalisant les
paramètres](<../1. Premiers pas/1.3. Personnaliser les paramètres.md>).