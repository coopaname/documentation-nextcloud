Personnaliser les paramètres
============================

Cliquez sur l'icône ronde colorée portant vos initiales dans le coin
supérieur droit de la page.

![](choisir-parametres.png?fileId=6546#mimetype=image%2Fpng&hasPreview=true)

Choisissez "Paramètres" dans le menu déroulant.

<video width="640" height="480" controls>
  <source src="choisir-parametres.mp4" type="video/mp4">
</video>



&nbsp;

Vérifiez vos informations personnelles.

![Page des paramètres avec différents champs optionnels](Parametres.png?fileId=6596#mimetype=image%2Fpng&hasPreview=true)

1.  Une adresse email est nécessaire pour réinitialiser votre mot de
    passe et éventuellement recevoir des notifications. Les autres
    informations sont facultatives.

2.  À la place de vos initiales, vous pouvez déposer une photo qui sera affichée sous forme de
    pastille dans certains modules, comme les discussions (*chat*) ou
    les partages de documents. Vous pouvez également utiliser une image
    déjà présente dans votre espace.

3.  Assurez-vous que la langue et les paramètres régionaux (heure
    locale, début de semaine) sont bons.

4.  Vous pouvez ajoutez des informations (nom de votre organisation, sous-titre, biographie, réseaux sociaux…) qui apparaîtront sur votre
    profil.

    ![](profil.png?fileId=6616#mimetype=image%2Fpng&hasPreview=true)

L'enregistrement des paramètres est automatique, vous pouvez reprendre
vos activités.

> ⚠️ **Attention à votre bloqueur de publicités**
>
> Si vous utilisez un bloqueur de publicité, il peut bloquer certaines
> fonctionnalités de Nextcloud selon les filtres que vous avez choisis.
>
> Si c'est le cas, vous pouvez désactiver votre bloqueur de publicités
> pour l'adresse <https://nextcloud.coopaname.coop> (et seulement pour
> cette adresse). Coopaname ne mettra pas de publicités sur son serveur.

------------------------------------------------------------------------

➡️ Continuer en apprenant à [gérer ses documents en
ligne](<../2. Gérer ses documents en ligne/Readme.md>).