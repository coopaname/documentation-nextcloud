Premiers pas
============

Le portail de Coopaname rassemble tous les services numériques de la
coopérative, dont Nextcloud. Commencez par valider votre accès au
portail :

Retrouver son nom d'utilisateur-ice
-----------------------------------

Le nom pour se connecter est celui de **l'adresse de courriel que vous
utilisez pour vous connecter à Odoo**.

Choisir un nouveau mot de passe
-------------------------------

Il va falloir **créer un nouveau mot de passe** en suivant ce mode
d'emploi pour accéder à la fonctionnalité "Mot de passe
oublié".

1.  Rendez-vous sur la page suivante :
     <a href="https://auth.coopaname.coop/resetpwd" title="Page « Mot de passe oublié » du portail de Coopaname (nouvelle fenêtre)" target="_blank">auth.coopaname.coop/resetpwd</a> 

2.  Remplissez les champs demandés. Utilisez la **même adresse de courriel que pour vous connecter à
    Odoo**.

<figure>
<img src="saisir-adresse-mail.png?fileId=6346#mimetype=image%2Fpng&hasPreview=true" alt="Page « Mot de passe oublié ? » du portail de Coopaname" />
<figcaption><i>Page "Mot de passe oublié ?" du portail de Coopaname. Un bandeau précise "merci de saisir votre adresse mail". Deux champs à remplir : l'adresse mail et un captcha. Un bouton "envoyez-moi un lien".</i></figcaption>
</figure>

3.  Cliquez sur **"Envoyez-moi un lien"**. Une page de confirmation de l'envoi du lien par mail s'affiche.

	![](mail-envoye.png?fileId=6337#mimetype=image%2Fpng&hasPreview=true)

4.  Ouvrez le mail qui vous a été envoyé. Cliquez sur **"Cliquez ici pour réinitialiser votre mot de passe"**

	![](reinitialiser-mot-de-passe.png?fileId=6326#mimetype=image%2Fpng&hasPreview=true)

5.  Entrez un nouveau mot de passe et cliquez sur "Envoyer".

<figure>
<img src="saisir-nouveau-mot-de-passe.png?fileId=6327#mimetype=image%2Fpng&hasPreview=true" alt="Page « Changez votre mot de passe » du portail de Coopaname" />
<figcaption><i>Page "Changez votre mot de passe" du portail de Coopaname. Un bandeau précise "merci de saisir votre nouveau mot de passe". Deux champs à remplir : le nouveau mot de passe et sa confirmation. Une case optionnelle : "Générer le mot de passe automatiquement". Un bouton "Envoyer".</i></figcaption>
</figure>

6.  Vous pouvez maintenant **vous connecter** à l'ensemble des services
    de Coopaname avec votre adresse mail et le mot de passe que vous venez de
    choisir.

------------------------------------------------------------------------

➡️ Continuez en apprenant comment [se
connecter](<../1. Premiers pas/1.1. Se connecter (sur le web).md>).