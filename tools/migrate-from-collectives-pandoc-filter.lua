--- Environment
local orig_dir = os.getenv("ORIG_DIR") or "orig"
local depth = os.getenv("DEPTH") or 1
local title = os.getenv("TITLE") or nil
local debug_enabled = os.getenv("DEBUG") or false

--- Check if a file or directory exists in this path
function exists(file)
   local ok, err, code = os.rename(file, file)
   if not ok then
      if code == 13 then
         -- Permission denied, but it exists
         return true
      end
   end
   return ok, err
end

--- urldecode
local hex_to_char = function(x)
  return string.char(tonumber(x, 16))
end

local urldecode = function(url)
  if url == nil then
    return
  end
  url = url:gsub("%%(%x%x)", hex_to_char)
  return url
end

--- urlencode
local char_to_hex = function(c)
  return string.format("%%%02X", string.byte(c))
end

local function urlencode(url)
  if url == nil then
    return
  end
  url = url:gsub("\n", "\r\n")
  url = url:gsub("([^%w/])", char_to_hex)
  return url
end

--- debug
local function log(str)
  if debug_enabled and debug_enabled ~= "" then
    io.stderr:write(str)
  end
end

--- Replacer
function Pandoc(el)
  if not title then
    return el
  end

  h1 = pandoc.Header(1, title)
  el.blocks:insert(1, h1)
  return el
end

function Link(el)
  target = urldecode(el.target)
  if target:find("nextcloud.coopaname.coop/apps/collectives/Documentation Nextcloud/") == nil then
    return el
  end

  target = target:gsub("https://nextcloud.coopaname.coop/apps/collectives/Documentation Nextcloud/", ""):gsub("?fileId=.*$", "")
  -- does this link to a directory?
  log(string.format("test if %s is a directory\n", orig_dir.."/"..target.."/"))
  if exists(orig_dir.."/"..target.."/") then
    target = target.."/Readme.md"
  else
    target = target..".md"
  end
  if not exists(orig_dir.."/"..target) then
    io.stderr:write(string.format("ERROR: Link to non-existent page “%s”\n", target))
  end
  target = string.rep("../", depth - 1)..target
  log(string.format("new target %s\n", target))
  el.target = "<"..target..">" -- urlencode(target)
  return el
end
